"use strict";

var Promise = require("bluebird");
var uuid = require("node-uuid");
var google = require("googleapis");
var request = require("request");
var stormy = require("stormy");

var googleOAuth = google.oauth2({version: "v2"});
var googlePlus = google.plus({version: "v1"});
var googleTokenInfoPromise = Promise.promisify(googleOAuth.tokeninfo);
var googleGetUser = Promise.promisify(googlePlus.people.get);

request.getPromise = Promise.promisify(request.get);

var checkAuthorization = function (req, res, next) {
	var self = this;
	var token = req.header("authorization");
	if (!token || !/^[a-z0-9\-]{36}$/.test(token)) {
		res.writeHead(401);
		res.end();
		return;
	}

	var userData = this._cache.get(token);
	if (userData === false) {
		res.writeHead(498);	// Expired token
		res.end();
	} else if (!userData) {
		this._model
			.getUser(null, null, token, null)
			.done(
				function (user) {
					if (!user) {
						res.writeHead(401);
						res.end();
						return;
					}

					if (self._isExpired(user.expiration)) {
						res.writeHead(498);	// Expired token
						res.end();
					} else {
						self._cache.set(token, user);
						req.session = user;
						next();
					}
				},
				function () {
					res.writeHead(401);
					res.end();
				}
			);
	} else {
		if (self._isExpired(userData.expiration)) {
			res.writeHead(498);	// Expired token
			res.end();
		} else {
			req.session = userData;
			next();
		}
	}
};

var OAuth2 = function (_options) {
	var options = _options || {};

	this._facebookAppId = options.facebookAppId || null;
	this._googleAppId = options.googleAppId || null;

	this._model = options.model || {
			renewTokens: function (accessToken, expiration, refreshToken, userId) {return Promise.reject();},
			getUser: function (email, password, accessToken, refreshToken) {return Promise.reject();}
		};

	this._googleOAuthClient = options.googleOAuthClient || null;

	this._accessTokenTime = options.accessTokenTime || 3600;

	this._cache = options.cache || new stormy({
			expiration: 600
		});
	
	// ExpressJS Middleware
	this.checkAuthorization = checkAuthorization.bind(this);

	return this;
};

OAuth2.prototype._generateTokens = function () {
	return {
		accessToken: uuid.v4(),
		refreshToken: uuid.v4(),
		expiration: Math.round(Date.now() / 1000) + this._accessTokenTime
	};
};

OAuth2.prototype._isExpired = function (expiration) {
	if (!expiration) {
		return true;
	}

	return (Math.round(Date.now() / 1000) - expiration) >= 0;
};

OAuth2.prototype._generateAccessToken = function () {
	return {
		accessToken: uuid.v4(),
		expiration: Math.round(Date.now() / 1000) + this._accessTokenTime
	};
};

OAuth2.prototype._renewTokens = function (userId) {
	var tokens = this._generateTokens();

	return this._model
		.renewTokens(tokens.accessToken, tokens.expiration, tokens.refreshToken, userId)
		.return(tokens);
};

OAuth2.prototype._renewAccessToken = function (userId) {
	var tokens = this._generateAccessToken();

	return this._model
		.renewTokens(tokens.accessToken, tokens.expiration, null, userId)
		.return(tokens);
};

OAuth2.prototype._grantFacebook = function (token) {
	var self = this;
	var facebookUser;

	return request.getPromise({
		url: "https://graph.facebook.com/v2.2/app",
		qs: {
			access_token: token,
			format: "json"
		},
		json: true
	}).spread(
		function (response, app) {
			if (app.id !== self._facebookAppId) {
				throw false;
			}

			return request.getPromise({
				url: "https://graph.facebook.com/v2.2/me",
				qs: {
					access_token: token,
					format: "json"
				},
				json: true
			});
		}
	).spread(
		function (response, user) {
			facebookUser = user;

			return database.queryPromise("SELECT id, access_token FROM users WHERE email = ?", [user.email]);
		}
	).spread(
		function (rows) {
			if (rows.length > 0) {
				if (self._cache.has(rows[0].access_token)) {
					self._cache.set(rows[0].access_token, false);
				}
				return OAuth2.renewTokens(rows[0].id)
			}

			return database.queryPromise("INSERT INTO users (facebook_id, email, nickname) VALUES (?, ?, ?)", [facebookUser.id, facebookUser.email, facebookUser.name]).spread(
				function (rows) {
					return OAuth2.renewTokens(rows.insertId);
				}
			);
		}
	)
};

OAuth2.prototype._grantGooglePlus = function (token) {
	var self = this;
	var googleUser;

	return googleTokenInfoPromise({access_token: token}).spread(
		function (tokenInfo) {
			if (tokenInfo.audience !== self._googleAppId) {
				throw false;
			}

			var client = googleOAuthClient();
			client.setCredentials({
				access_token: token
			});

			return googleGetUser({userId: "me", auth: client});
		}
	).spread(
		function (user) {
			googleUser = user;

			var len = googleUser.emails.length;
			var email;
			for (var i = 0; i < len; i++) {
				if (googleUser.emails[i].type === "account") {
					email =  googleUser.emails[i].value;
					break;
				}
			}
			if (!email) {
				throw false;
			}

			googleUser.email = email;

			return database.queryPromise("SELECT id, access_token FROM users WHERE email = ?", [email]);
		}
	).spread(
		function (rows) {
			if (rows.length > 0) {
				if (self._cache.has(rows[0].access_token)) {
					self._cache.set(rows[0].access_token, false);
				}
				return OAuth2.renewTokens(rows[0].id)
			}

			return database.queryPromise("INSERT INTO users (google_plus_id, email, nickname) VALUES (?, ?, ?)", [googleUser.id, googleUser.email, googleUser.displayName]).spread(
				function (rows) {
					return OAuth2.renewTokens(rows.insertId);
				}
			);
		}
	);
};

OAuth2.prototype._grantPassword = function (email, password) {
	var self = this;
	var user;

	return this._model
		.getUser(email, password, null, null)
		.then(
			function (_user) {
				user = _user;
				if (!user) {
					throw false;
				}

				return self._renewTokens(user.id);
			}
		).tap(
			function (tokens) {
				// Mark as expired
				self._cache.set(user.accessToken, false);

				user.accessToken = tokens.accessToken;
				self._cache.set(user.accessToken, user);
			}
		);
};

OAuth2.prototype._grantRefreshToken = function (token) {
	var self = this;
	var user;

	return this._model
		.getUser(null, null, null, token)
		.then(
			function (_user) {
				user = _user;
				if (!user) {
					throw false;
				}

				return self._renewAccessToken(user.id);
			}
		).tap(
			function (tokens) {
				// Mark as expired
				self._cache.set(user.accessToken, false);

				user.accessToken = tokens.accessToken;
				self._cache.set(user.accessToken, user);
			}
		);
};

OAuth2.prototype.authorize = function (data) {
	var grantType = data.grant;
	var token;

	switch (grantType) {
		/*case "facebook":
			if (this._facebookAppId === null) {
				return Promise.reject("Facebook APP ID needs to be set.");
			}

			token = data.accessToken;
			return this._grantFacebook(token);
		case "gplus":
			if (this._googleAppId === null) {
				return Promise.reject("Google APP ID needs to be set.");
			}

			if (this._googleOAuthClient === null) {
				return Promise.reject("Google OAuth client needs to be set.");
			}

			token = data.accessToken;
			return this._grantGooglePlus(token);*/
		case "password":
			return this._grantPassword(data.email, data.password);
		case "refreshToken":
			token = data.token;
			return this._grantRefreshToken(token);
		default:
			return Promise.reject("Invalid grant type.");
	}
};

module.exports = OAuth2;